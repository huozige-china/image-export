﻿var MyImageCellType = (function (_super) {
    __extends(MyImageCellType, _super);
    function MyImageCellType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
 
    MyImageCellType.prototype.createContent = function () {
        var self = this;
 
        var element = this.CellElement;
        var cellTypeMetaData = element.CellType;
        var container = $("<div id='" + this.ID + "'></div>");
 
        if (cellTypeMetaData.ImageInfo) {
            var imageDiv = $("<div></div>");
            if (cellTypeMetaData.ImageInfo.BuiltIn) {
                var imagePath = Forguncy.Helper.SpecialPath.getBuiltInImageFolderPath() + cellTypeMetaData.ImageInfo.Name;
            } else {
                imagePath = Forguncy.Helper.SpecialPath.getImageEditorUploadImageFolderPath() + cellTypeMetaData.ImageInfo.Name;
            }
 
            imageDiv.css("background-image", "url('" + imagePath + "')");
            imageDiv.css("background-position", "center");
            imageDiv.css("background-size", "contain");
            imageDiv.css("background-repeat", "no-repeat");
            imageDiv.css("width", "100%");
            imageDiv.css("height", element.Height);
 
            container.append(imageDiv);
        }
         
 
        return container;
    };
 
    MyImageCellType.prototype.getValueFromElement = function () {
        return null;
    };
 
    MyImageCellType.prototype.setValueToElement = function (element, value) {
 
    };
 
    MyImageCellType.prototype.disable = function () {
        _super.prototype.disable.call(this);
    };
 
    MyImageCellType.prototype.enable = function () {
        _super.prototype.enable.call(this);
    };
 
    return MyImageCellType;
}(Forguncy.CellTypeBase));
 
// Key format is "Namespace.ClassName, AssemblyName"
Forguncy.CellTypeHelper.registerCellType("MyImageCellType.MyImageCellType, MyImageCellType", MyImageCellType);
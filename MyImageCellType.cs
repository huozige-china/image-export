﻿using GrapeCity.Forguncy.CellTypes;
using GrapeCity.Forguncy.Plugin;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace MyImageCellType
{
    [Designer("MyImageCellType.MyImageCellTypeDesigner,MyImageCellType")]
        public class MyImageCellType : CellType, IExportCellType
        {
            public ImageValue ImageInfo
            {
                get; set;
            }

        public bool ExportPicture => true;

        public ExportResultInfo ExportToExcel(ICellInfo targetCell, IExportContext context)
        {
            var result = new ExportResultInfo();
            if (this.ImageInfo != null)
            {
                var folderName = context.DrawingHelper.ForguncyImageEditorFolderPath;
                if (this.ImageInfo.BuiltIn)
                {
                    folderName = context.DrawingHelper.ForguncyBuiltInImagesFolderPath;
                }
                var imagePath = context.ExportImageContext.GetServerPathFunc("~/" + folderName + "/" + this.ImageInfo.Name);
                var imageSource = context.GetPictureByPicturePath(imagePath, context.PictureSize.Width, context.PictureSize.Height);
                result.ExportPicture = imageSource;
            }
            return result;
        }
    }
        public class MyImageCellTypeDesigner : CellTypeDesigner<MyImageCellType>
        {
            public override FrameworkElement GetDrawingControl(ICellInfo cellInfo, IDrawingHelper drawingHelper)
            {
                Grid container = new Grid();
                if (this.CellType.ImageInfo != null)
                {
                    //uploaded image in ImageSelectorEditor
                    var imagePath = Path.Combine(drawingHelper.ForguncyImageEditorFolderPath, this.CellType.ImageInfo.Name);
                    if (this.CellType.ImageInfo.BuiltIn)
                    {
                        //builtin image in ImageSelectorEditor
                        imagePath = Path.Combine(drawingHelper.ForguncyBuiltInImagesFolderPath, this.CellType.ImageInfo.Name);
                    }
                    Image image = new Image();
                    try
                    {
                        image.Source = new BitmapImage(new Uri(imagePath, UriKind.RelativeOrAbsolute));
                    }
                    catch (Exception)
                    {
                        //不支持预览svg格式的图片
                    }
                    image.Stretch = System.Windows.Media.Stretch.Uniform;
                    image.VerticalAlignment = VerticalAlignment.Center;
                    image.HorizontalAlignment = HorizontalAlignment.Center;
                    container.Children.Add(image);
                }

                return container;
            }
        }
    }
